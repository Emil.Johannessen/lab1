package rockPaperScissors;

import java.util.*;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }

    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        while (true) {
            System.out.println("Let's play round " + roundCounter);


            while (true) {
                System.out.println("Your choice (Rock/Paper/Scissors)? ");
                String userChoice = sc.nextLine().toLowerCase();

                Random random = new Random();
                String computerChoice = rpsChoices.get(random.nextInt(rpsChoices.size()));

                if (userChoice.equals(computerChoice)) {
                    System.out.printf("Human chose %s, computer chose %s. Its a tie!\n", userChoice, computerChoice);
                    //System.out.printf("Score: human %d, computer %d\n", humanScore, computerScore);
                    break;
                }

                if (userChoice.equals("rock") && computerChoice.equals("scissors")) {
                    System.out.printf("Human chose %s, computer chose %s. Human wins!\n", userChoice, computerChoice);
                    humanScore++;
                    //roundCounter++;
                    break;
                }
                if (userChoice.equals("paper") && computerChoice.equals("rock")) {
                    System.out.printf("Human chose %s, computer chose %s. Human wins!\n", userChoice, computerChoice);
                    humanScore++;
                    //roundCounter++;
                    break;
                }
                if (userChoice.equals("scissor") && computerChoice.equals("paper")) {
                    System.out.printf("Human chose %s, computer chose %s. Human wins!\n", userChoice, computerChoice);
                    humanScore++;
                    break;
                    }

                if (userChoice.equals("scissors") && computerChoice.equals("rock")){
                    System.out.printf("Human chose %s, computer chose %s. Computer wins wins!\n", userChoice, computerChoice);
                    computerScore++;
                    break;
                }

                if (userChoice.equals("rock") && computerChoice.equals("paper")){
                    System.out.printf("Human chose %s, computer chose %s. Computer wins!\n", userChoice, computerChoice);
                    computerScore++;
                    break;
                }

                if (userChoice.equals("paper") && computerChoice.equals("scissors")){
                    System.out.printf("Human chose %s, computer chose %s. Computer wins!\n", userChoice, computerChoice);
                    computerScore++;
                    break;
                }
                if (!userChoice.equals("rock") || !userChoice.equals("paper") || !userChoice.equals("Scissors")) {
                System.out.printf("I do not understand %s. Could you try again?\n", userChoice);
                continue;
                }

            }
                System.out.printf("Score: human %d, computer %d\n", humanScore, computerScore);
                roundCounter++;

                System.out.print("Do you wish to continue playing? (y/n)?\n ");
                String playAgain = sc.nextLine();

                if (playAgain.equals("n")) {
                    System.out.println("Bye bye :)");
                    break;
                }

            }
        }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }
}
